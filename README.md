# README #

### What is this repository for? ###

This is a playground for learning CDO. It is a maven project consisting of 3 modules

* an EMF model (xcore) 
* a core bundle containg code for the CDO client and the CDO server
* a test fragment bundle containing unit tests.

### How do I get set up? ###

* Clone the repository (git clone git@bitbucket.org:sscheiber/cdo-arena.git)
* Build (mvn clean install)
* And/Or import into Eclipse (Import existing maven project)