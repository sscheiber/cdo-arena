package ro.scheiber.arena.cdo.core.test;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.eresource.CDOResourceFolder;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.net4j.db.DBException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ro.scheiber.arena.cdo.client.MyCdoClient;
import ro.scheiber.arena.cdo.server.MyCdoServer;
import ro.scheiber.arena.model.library.Book;
import ro.scheiber.arena.model.library.Library;
import ro.scheiber.arena.model.library.LibraryFactory;
import ro.scheiber.arena.model.library.LibraryPackage;
import ro.scheiber.arena.model.library.Writer;

public class CdoTest {
    private final MyCdoServer server = new MyCdoServer();
    private final MyCdoClient client = new MyCdoClient("repo");
    private final MyCdoClient client2 = new MyCdoClient("repo2");

    @Before
    public void setup() {
        server.start();
        client.connect();
        client.addPackage(LibraryPackage.eINSTANCE);
        client2.connect();
        client2.addPackage(LibraryPackage.eINSTANCE);
    }

    @After
    public void tearDown() {
        client.disconnect();
        client2.disconnect();
        server.stop();
        try {
            server.purge();
        } catch (DBException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCdoQueryLatest() {
        client.commit("commit v1", "mylib", this::createTestModel);
        client.commit("commit v2", "mylib", this::updateTestModel);

        List<Book> books = client.query("Book.allInstances()->select(b: Book | b.author.name = 'Stephen King')", null, Book.class);
        assertEquals(1, books.size());
        assertEquals("Stephen King", books.get(0).getAuthor().getName());
    }

    @Test
    public void testCdoQueryAggregateLatest() {
        client.commit("commit v1", "mylib", this::createTestModel);
        client.commit("commit v2", "mylib", this::updateTestModel);

        int authors = client.queryValue("Writer.allInstances()->size()", null, Integer.class);
        assertEquals(2, authors);
    }

    @Test
    public void testCdoQueryProjectionLatest() {
        client.commit("commit v1", "mylib", this::createTestModel);
        client.commit("commit v2", "mylib", this::updateTestModel);

        List<String> titles = client.query("Book.allInstances()->collect(title)", null, String.class);
        assertEquals(3, titles.size());
    }

    @Test
    public void test2RepoCommit() {
        client.commit("commit v1", "mylib", this::createTestModel);
        client2.commit("commit v1", "mylib", this::createTestModel);
        client.commit("commit v2", "mylib", this::updateTestModel);

        List<String> titles = client.query("Book.allInstances()->collect(title)", null, String.class);
        assertEquals(3, titles.size());

        List<String> titles2 = client2.query("Book.allInstances()->collect(title)", null, String.class);
        assertEquals(2, titles2.size());
    }

    @Test
    public void testA() {
        client.commit("commit v1", "project1/mylib", this::createTestModel);
        client.commit("commit v2", "project2/mylib", this::createTestModel);
        client.commit("commit v3", "project1/mylib", this::updateTestModel);
        client.commit("commit v4", "project2/mylib", this::updateTestModel);

        CDOView view = client.getView();
        view.getHistory().size();
        Arrays.stream(view.getHistory().getElements()).forEach(commit -> System.out.println(commit.getComment()));
        CDOResourceFolder resourceFolder = view.getResourceFolder("project1");
        Arrays.stream(resourceFolder.cdoHistory().getElements()).forEach(commit -> System.out.println(commit.getComment()));
        CDOResource resource = (CDOResource) resourceFolder.eContents().get(0);
        Arrays.stream(resource.cdoHistory().getElements()).forEach(commit -> System.out.println(commit.getComment()));
    }

    private void createTestModel(final CDOResource resource) {
        Library library = LibraryFactory.eINSTANCE.createLibrary();
        Writer writer = LibraryFactory.eINSTANCE.createWriter();
        writer.setName("Stephen King");
        library.getWriters().add(writer);
        Book book1 = LibraryFactory.eINSTANCE.createBook();
        book1.setTitle("The Shining");
        book1.setAuthor(writer);
        Book book2 = LibraryFactory.eINSTANCE.createBook();
        book2.setTitle("The Shunned House");
        book2.setAuthor(writer);
        library.getBooks().add(book1);
        library.getBooks().add(book2);
        resource.getContents().add(library);
    }

    private void updateTestModel(final CDOResource resource) {
        Library library = (Library) resource.getContents().get(0);
        Writer writer = LibraryFactory.eINSTANCE.createWriter();
        writer.setName("H.P. Lovecraft");
        library.getWriters().add(writer);
        Book book1 = LibraryFactory.eINSTANCE.createBook();
        book1.setTitle("The Call of Chtulu");
        book1.setAuthor(writer);
        library.getBooks().add(book1);

        Book book2 = library.getBooks()
                .stream()
                .filter(book -> book.getTitle().equals("The Shunned House"))
                .findAny()
                .get();
        book2.setAuthor(writer);
    }
}
