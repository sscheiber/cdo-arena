package ro.scheiber.arena.cdo.client;

import java.util.List;
import java.util.function.Consumer;

import org.eclipse.emf.cdo.common.commit.CDOCommitHistory;
import org.eclipse.emf.cdo.common.commit.CDOCommitInfo;
import org.eclipse.emf.cdo.eresource.CDOResource;
import org.eclipse.emf.cdo.net4j.CDONet4jSession;
import org.eclipse.emf.cdo.net4j.CDONet4jSessionConfiguration;
import org.eclipse.emf.cdo.net4j.CDONet4jUtil;
import org.eclipse.emf.cdo.transaction.CDOTransaction;
import org.eclipse.emf.cdo.util.CommitException;
import org.eclipse.emf.cdo.view.CDOAdapterPolicy;
import org.eclipse.emf.cdo.view.CDOQuery;
import org.eclipse.emf.cdo.view.CDOView;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.net4j.Net4jUtil;
import org.eclipse.net4j.connector.IConnector;
import org.eclipse.net4j.util.WrappedException;
import org.eclipse.net4j.util.container.IPluginContainer;

public class MyCdoClient {

    private final String repository;
    private CDONet4jSession session;
    private CDOView view;

    public MyCdoClient(final String repository) {
        this.repository = repository;
    }

    public void connect() {
        IConnector connector = Net4jUtil.getConnector(IPluginContainer.INSTANCE, "tcp", "localhost:22233");

        CDONet4jSessionConfiguration config = CDONet4jUtil.createNet4jSessionConfiguration();
        config.setConnector(connector);
        config.setRepositoryName(repository);
        config.setUserID("stef");
        config.setSignalTimeout(300000);

        session = config.openNet4jSession();
        view = session.openView();
        view.options().addChangeSubscriptionPolicy(CDOAdapterPolicy.ALL);
    }

    public CDOCommitHistory getHistory() {
        return session.getCommitInfoManager().getHistory();
    }

    public CDOView getView() {
        return view;
    }

    public CDOView getView(final CDOCommitInfo commit) {
        return session.openView(commit.getBranch(), commit.getTimeStamp());
    }

    public <T> List<T> query(final CDOView view, final String queryString, final Object context, final Class<T> resultType) {
        CDOQuery query = view.createQuery("ocl", queryString, context);
        return query.getResult(resultType);
    }

    public <T> List<T> query(final String queryString, final Object context, final Class<T> resultType) {
        return query(view, queryString, context, resultType);
    }

    public <T> T queryValue(final String queryString, final Object context, final Class<T> resultType) {
        CDOQuery query = view.createQuery("ocl", queryString, context);
        query.setParameter("cdoLazyExtents", false);
        return query.getResultValue(resultType);
    }

    public CDOCommitInfo commit(final String commitMessage, final String resourcePath, Consumer<CDOResource> resourceUpdater) {
        CDOTransaction transaction = session.openTransaction();
        try {
          CDOResource resource = transaction.getOrCreateResource(resourcePath);
          resourceUpdater.accept(resource);
          transaction.setCommitComment(commitMessage);
          return transaction.commit();
        }
        catch (CommitException ex)
        {
          throw WrappedException.wrap(ex);
        }
        finally
        {
          transaction.close();
        }
    }

    public void addPackage(final EPackage ePackage) {
        if (isConnected()) {
            session.getPackageRegistry().putEPackage(ePackage);
        }
    }

    public void disconnect() {
        if (isConnected()) {
            session.close();
        }
    }

    private boolean isConnected() {
        return session != null && !session.isClosed();
    }
}
